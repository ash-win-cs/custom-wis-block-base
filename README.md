# Custom WIS Block Base 


Content
  - Introduction
  - Data-sheets
  - References


<br>

## Introduction
- WisBlock Base is the carrier board for the power supply and the inter-connection between the Core, the Sensor and the IO modules.
- WisBlock Core is the data processing unit with BLE and LoRa connectivity.
- WisBlock IO are interface modules that extend the communication capabilities of WisBlock
- WisBlock Sensor are modules that carry different types of sensors with I2C interface.

## Connectors
- 24 pin Connector for Sensor Slot
- 40 pin Connector for Core & IO Slot

## References

- Rakwireless documentation to getting started : https://news.rakwireless.com/how-to-create-custom-wisblock-sensor-or-wisblock-io-module/

